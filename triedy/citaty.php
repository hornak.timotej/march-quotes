<?php

class Citaty {

	public $den;

	public function __construct($den) {

		$this->den = $den;
	}

	public function vypisCitat() {

		switch ($this->den) {
			case '1.':
				$vysledok = '<p class="font-italic">
							„Stratiť lásku je strašné. Klame, kto vraví, že smrť je horšia.“
							</p>
							<footer class="blockquote-footer">Bob Marley</footer>
				';
				break;

			case '2.':
				$vysledok = '<p class="font-italic">
							„Práve tomu, kto by dokázal celý život niekoho milovať, to nie je dopriané.“
							</p>
							<footer class="blockquote-footer">Edmnondo De Amicis</footer>
							';
				break;

			case '3.':
				$vysledok = '<p class="font-italic">
							„Umenie života spočíva v tom, že sa učíme trpieť a usmievať sa.“
							</p>
							<footer class="blockquote-footer">Hermann Hesse</footer>
							';
				break;

			case '4.':
				$vysledok = '<p class="font-italic">
							„Rodina je miestom lásky a života. Miestom, kde láska rodí život.“
							</p>
							<footer class="blockquote-footer">Ján Pavol II.</footer>
							';
				break;

			case '5.':
				$vysledok = '<p class="font-italic">
							„Keď netrénujete, pamätajte si, že niekto iný trénuje, a keď sa s ním stretnete, tak vás porazí.“
							</p>
							<footer class="blockquote-footer">Bill Bradley</footer>
							';
				break;

			case '5.':
				$vysledok = '<p class="font-italic">
							„Každý otrok má vo svojich rukách silu ukončiť svoje zajatie.“
							</p>
							<footer class="blockquote-footer">Willian Shakespeare</footer>
							';
				break;

			case '7.':
				$vysledok = '<p class="font-italic">
							„Otec mi nikdy nedovolil, aby sa čokoľvek, čo som sa naučil, stalo len cvičením mojej pamäte.“
							</p>
							<footer class="blockquote-footer">John Stuart Mill</footer>
							';
				break;

			case '8.':
				$vysledok = '<p class="font-italic">
							„Smutným aspektom súčasného života je, že veda objavuje nové poznatky rýchlejšie, ako spoločnosť získava rozum.“
							</p>
							<footer class="blockquote-footer">Isaac Asimov</footer>
							';
				break;

			case '9.':
				$vysledok = '<p class="font-italic">
							„Lož precestuje pol sveta, zatiaľ čo pravda si len zaväzuje šnúrky.“
							</p>
							<footer class="blockquote-footer">Mark Twain</footer>
							';
				break;

			case '10.':
				$vysledok = '<p class="font-italic">
							„Ľudia nie sú buď šľachetní alebo skazení. Sú ako šalát, ktorý má v sebe nakrájané dobré aj zlé a keď sa to všetko zmieša, vzniká zmätok a konflikt.“
							</p>
							<footer class="blockquote-footer">Lemony Snicket</footer>
							';
				break;

			case '11.':
				$vysledok = '<p class="font-italic">
							„Občas tak dlho pozeráme na zatvárajúce sa dvere, že príliš neskoro zbadáme tie, ktorú sú otvorené.“
							</p>
							<footer class="blockquote-footer">Alexander G. Bell</footer>
							';
				break;

			case '12.':
				$vysledok = '<p class="font-italic">
							„Vždy to vyzerá ako nemožné, až pokým to niekto nespraví.“
							</p>
							<footer class="blockquote-footer">Nelson Mandela</footer>
							';
				break;

			case '13.':
				$vysledok = '<p class="font-italic">
							„Vzdelávanie nie je učenie faktov, ale trénovanie mysle, aby myslela.“
							</p>
							<footer class="blockquote-footer">Albert Einstein</footer>
							';
				break;

			case '14.':
				$vysledok = '<p class="font-italic">
							„Dobre urobené je lepšie ako dobre povedané.“
							</p>
							<footer class="blockquote-footer">Benjamin Franklin</footer>
							';
				break;

			case '15.':
				$vysledok = '<p class="font-italic">
							„Už je to dlho, čo som si všimol, že ľudia úspechu len zriedka vysedávali a dúfali, že sa im veci prihodia. Vyšli von a boli to oni, kto sa prihodil veciam.“
							</p>
							<footer class="blockquote-footer">Leonardo da Vinci</footer>
							';
				break;

			case '16.':
				$vysledok = '<p class="font-italic">
							„Život nie je o hľadaní samého seba. Je o tvorení samého seba.“
							</p>
							<footer class="blockquote-footer">George Bernard Shaw</footer>
							';
				break;

			case '17.':
				$vysledok = '<p class="font-italic">
							„Naša najväčšia sláva nespočíva v tom, že nikdy nespadnete, ale v tom, že sa postavíte každý jeden krát, keď spadnete.“
							</p>
							<footer class="blockquote-footer">Oliver Goldsmith</footer>
							';
				break;

			case '18.':
				$vysledok = '<p class="font-italic">
							„Poznať samého seba je začiatkom všetkej múdrosti.“
							</p>
							<footer class="blockquote-footer">Aristoteles</footer>
							';
				break;

			case '19.':
				$vysledok = '<p class="font-italic">
							„Poznať iných je znakom inteligencie, poznať seba je znakom múdrosti. Ovládať ostatných je znakom sily, ale ovládať seba je skutočná moc.“
							</p>
							<footer class="blockquote-footer">Lao Tzu</footer>
							';
				break;

			case '20.':
				$vysledok = '<p class="font-italic">
							„Blázni majú vo zvyku veriť, že všetko čo napíše slávny autor je hodné obdivu. No ja čítam len to, čo sa páči mne a vyhovuje môjmu vkusu.“
							</p>
							<footer class="blockquote-footer">Voltaire</footer>
							';
				break;

			case '21.':
				$vysledok = '<p class="font-italic">
							„Buďte vďační za ľudí, ktorí vás robia šťastnými, sú čarovným záhradníkom, pri ktorom vaša duša previtá.“
							</p>
							<footer class="blockquote-footer">Marcel Proust</footer>
							';
				break;

			case '22.':
				$vysledok = '<p class="font-italic">
							„Všetci sa rodíme ako umelci. Problém je ostať umelcom, keď vyrastieme.“
							</p>
							<footer class="blockquote-footer">Picasso</footer>
							';
				break;

			case '23.':
				$vysledok = '<p class="font-italic">
							„Príliš veľa ľudí míňa zarobené peniaze na veci, ktoré nechcú, aby urobili dojem na ľudí, ktorých nemajú radi.“
							</p>
							<footer class="blockquote-footer">Will Rogers</footer>
							';
				break;

			case '24.':
				$vysledok = '<p class="font-italic">
							„Život plný chýb je viac hodný uznania ako život, v ktorom ste nič neurobili.“
							</p>
							<footer class="blockquote-footer">George Bernard Shaw</footer>
							';
				break;

			case '25.':
				$vysledok = '<p class="font-italic">
							„Máte moc nad svojou mysľou, ale nie externými udalosťami. Uvedomte si to a nájdete silu.“
							</p>
							<footer class="blockquote-footer">Marcus Aurelius</footer>
							';
				break;

			case '26.':
				$vysledok = '<p class="font-italic">
							„Stretnutie dvoch ľudí je ako kontakt dvoch chemických prvkov. Ak vznikne nejaká reakcia, oba sa transformujú.“
							</p>
							<footer class="blockquote-footer">Carl G. Jung</footer>
							';
				break;

			case '27.':
				$vysledok = '<p class="font-italic">
							„Nebojte sa dokonalosti – nikdy ju nedosiahnete.“
							</p>
							<footer class="blockquote-footer">Salvador Dali</footer>
							';
				break;

			case '28.':
				$vysledok = '<p class="font-italic">
							„Často stretnete svoj osud na ceste, ktorej ste sa rozhodli vyhnúť.“
							</p>
							<footer class="blockquote-footer">Goldie Hawn</footer>
							';
				break;

			case '29.':
				$vysledok = '<p class="font-italic">
							„Šťastie nie je neprítomnosť problémov. Je to schopnosť sa s nimi vysporiadať.“
							</p>
							<footer class="blockquote-footer">Steve Maraboli</footer>
							';
				break;

			case '30.':
				$vysledok = '<p class="font-italic">
							„Nech prečítate akokoľvek veľa svätých slov, alebo akokoľvek veľa ich poviete, k čomu vám to bude, ak podľa nich nebudete aj konať?“
							</p>
							<footer class="blockquote-footer">Buddha</footer>
							';
				break;

			case '31.':
				$vysledok = '<p class="font-italic">
							„Posledných 33 rokov som sa pozeral každé ráno do zrkadla a pýtal sa: ‘Ak by bol dnešný deň mojím posledným, robil by som to, čo dnes budem robiť?‘ A keď bola odpoveď nie nasledovala príliš často po sebe, vedel som, že niečo musím zmeniť. “
							</p>
							<footer class="blockquote-footer">Steve Jobs</footer>
							';
				break;

				
			
			default:
				$vysledok = "error";
				break;
		}
		return $vysledok;	
	}

}






?>