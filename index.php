<!DOCTYPE HTML>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>OOP</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

<!-- CSS -->
<link rel="stylesheet" href="stylesheet.css">

<body>

	<div class="text-white container text-center">

		<h1 class="display-1 text-center">Marec</h1>

		<h5 class="pb-5 text-white">Vyberte si dátum a prečítajte si citát dňa</h5>
		<form action="<?php $_SERVER["PHP_SELF"]?> " method="POST">
			<select name="den" id="den" data-show-subtext="true" class="selectpicker mb-4">
				<option value="1." data-subtext="Albín">1.</option>
				<option value="2." data-subtext="Anežka">2.</option>
				<option value="3." data-subtext="Buhomil/Bohumila">3.</option>
				<option value="4." data-subtext="Kazimír">4.</option>
				<option value="5." data-subtext="Fridrich">5.</option>
				<option value="6." data-subtext="Radoslav/Radoslava">6.</option>
				<option value="7." data-subtext="Tomáš">7.</option>
				<option value="8." data-subtext="Alan/Alana">8.</option>
				<option value="9." data-subtext="Františka">9.</option>
				<option value="10." data-subtext="Bruno/Branislav">10.</option>
				<option value="11." data-subtext="Angela/Angelika">11.</option>
				<option value="12." data-subtext="Gregor">12.</option>
				<option value="13." data-subtext="Vlastimil">13.</option>
				<option value="14." data-subtext="Matilda">14.</option>
				<option value="15." data-subtext="Svetlana">15.</option>
				<option value="16." data-subtext="Boleslav">16.</option>
				<option value="17." data-subtext="Ľubica">17.</option>
				<option value="18." data-subtext="Eduard">18.</option>
				<option value="19." data-subtext="Jozef">19.</option>
				<option value="20." data-subtext="Víťazoslav/Klaudius">20.</option>
				<option value="21." data-subtext="Blahoslav">21.</option>
				<option value="22." data-subtext="Beňadik">22.</option>
				<option value="23." data-subtext="Adrián">23.</option>
				<option value="24." data-subtext="Gabriel">24.</option>
				<option value="25." data-subtext="Marián">25.</option>
				<option value="26." data-subtext="Emanuel">26.</option>
				<option value="27." data-subtext="Alena">27.</option>
				<option value="28." data-subtext="Soňa">28.</option>
				<option value="29." data-subtext="Miroslav">29.</option>
				<option value="30." data-subtext="Vieroslava">30.</option>
				<option value="31." data-subtext="Benjamín">31.</option>

			</select>
			<br>

			<script type="text/javascript">
  				document.getElementById('den').value = "<?php echo $_POST['den'];?>";
			</script>

			<input type="submit" class="btn btn-outline-light" name="submit" value="ukazáť citát">
		</form>
	

		<blockquote class="blockquote pt-4">
<?php

require_once("triedy/Citaty.php");

if(isset($_POST['submit'])){
 $den = $_POST['den'];

 $citaty = new Citaty($den);
 echo $citaty->vypisCitat();
}

?>

		</blockquote>

	</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

</html>